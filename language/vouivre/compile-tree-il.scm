;;; Guile Scheme specification

;; Copyright (C) 2001, 2009, 2010 Free Software Foundation, Inc.

;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;;
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

;;;; This file has been modified by Vouivre Digital Corporation. The exact
;;;; modifications can be seen in a shell using:
;;;; $ git diff 5226e2890df9a25bd1d33514ff67539da7794905 compile-tree-il.scm

;;; Code:

(define-module (language vouivre compile-tree-il)
  #:use-module (language tree-il)
  #:use-module (srfi srfi-71)
  #:use-module (vouivre curry)
  #:export (compile-tree-il))

;;; environment := MODULE

(define (compile-tree-il x e opts)
  (save-module-excursion
   (lambda ()
     (set-current-module e)
     ;; TODO: Why do we need to use `(@@ (vouivre curry) symtab)' here instead of
     ;;       simply `symtab'? If we don't it always return an empty symtab.
     (let ((t expr (expand (@@ (vouivre curry) symtab) (syntax->datum x))))
       (let* ((x (macroexpand expr 'c '(compile load eval)))
              (cenv (current-module)))
	 (values x cenv cenv))))))
