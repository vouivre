;;;; Copyright (C) 2023 Vouivre Digital Corporation
;;;;
;;;; This file is part of Vouivre.
;;;;
;;;; Vouivre is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.
;;;;
;;;; Vouivre is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public
;;;; License along with Vouivre. If not, see <https://www.gnu.org/licenses/>.

(define-module (vouivre misc)
  #:use-module (ice-9 arrays)
  #:use-module (srfi srfi-1)
  #:export
  (array-map
   array-map-indexed
   flip
   for-indices-in-range
   if-let
   ifn
   list-zeros
   map-indexed
   produce-array
   produce-typed-array))

(define (flip f)
  "Returns a procedure behaving as `f', but with arguments taken in reverse
order."
  (lambda args
    (apply f (reverse args))))

(define-syntax if-let
  (syntax-rules ()
    [(_ (x test) consequent alternate)
     (let ([x test])
       (if x consequent alternate))]
    [(_ (x test) consequent)
     (let ([x test])
       (if x consequent))]))

(define-syntax ifn
  (syntax-rules ()
    [(_ test alternate consequent)
     (if test consequent alternate)]
    [(_ test alternate)
     (if (not test) alternate)]))

(define (list-zeros n)
  (list-tabulate n (lambda _ 0)))

(define (map-indexed f . lists)
  "Like `map' but the last argument of `f' is passed the corresponding index."
  (apply map f (append lists (list (list-tabulate (length (car lists))
						  identity)))))

(define (for-indices-in-range f starts ends)
  (define (for-indices-in-range% f indices starts ends)
    (if (null? starts)
	(apply f (reverse indices))
	(do ((i (car starts) (1+ i)))
	    ((= i (car ends)))
	  (for-indices-in-range%
	   f
           (cons i indices)
           (cdr starts)
           (cdr ends)))))
  (for-indices-in-range% f '() starts ends))

;;;; array utilities

(define (produce-typed-array f type . dims)
  (let ((a (apply make-typed-array type *unspecified* dims)))
    (array-index-map! a f)
    a))

(define (produce-array f . dims)
  (apply produce-typed-array f #t dims))

(define (array-map proc array . more)
  (let ((x (array-copy array)))
    (apply array-map! x proc array more)
    x))

(define (array-map-indexed proc array)
  (let ((x (array-copy array)))
    (array-index-map!
     x
     (lambda indices
       (apply proc
	      (apply array-ref array indices)
	      indices)))
    x))
