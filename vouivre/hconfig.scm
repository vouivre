(define-module
 (vouivre hconfig)
 #:use-module
 (srfi srfi-26)
 #:export
 (%version
  %author
  %license
  %copyright
  %gettext-domain
  G_
  N_
  init-nls
  init-locale))

(define %version "0.2.0")

(define %author "Vouivre Digital Corporation")

(define %license 'gpl3+)

(define %copyright '(2023))

