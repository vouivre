;;;; Copyright (C) 2023 Vouivre Digital Corporation
;;;;
;;;; This file is part of Vouivre.
;;;;
;;;; Vouivre is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.
;;;;
;;;; Vouivre is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public
;;;; License along with Vouivre. If not, see <https://www.gnu.org/licenses/>.

(define-module (vouivre curry)
  #:use-module ((ice-9 curried-definitions) :prefix c)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-71)
  #:use-module (vouivre misc)
  #:export
  (curried-untyped-define
   equal-types?
   expand
   parse
   symtab
   type-of
   ∷))

(define symtab '())

(define-record-type node
  (make-node content)
  node?
  (content node-content set-node-content!))

(define (type-error n . args)
  (let ((errors '((0 . TBD)
		  (1 . INCOMPATIBLE)
		  (2 . BAD-INPUT)
		  (3 . BAD-APPLICATION)
		  (4 . UNBOUND-SYMBOL)
		  (5 . TYPE-EXPECT-TYPES))))
    (apply error "type error" (assoc-ref errors n) args)))

(define (zo? x)
  "Predicate for the zero type."
  (and (number? x) (zero? x)))

(define (tv? x)
  "Predicate for type variables."
  (and (number? x) (positive? x) #t))

;; Return a unique type variable.
(define next
  (let ((count 1))
    (lambda ()
      (set! count (1+ count))
      (1- count))))

(define (copy-tree node)
  "Return a copy of the tree rooted in `node'."
  (let ((x (node-content node)))
    (if (pair? x)
	(make-node (cons (copy-tree (car x))
			 (copy-tree (cdr x))))
	(make-node x))))

(define (map-tree! proc nx)
  "Map `proc' in-place over the content of the leaves of `nx'."
  (let ((x (node-content nx)))
    (if (pair? x)
	(begin
	  (map-tree! proc (car x))
	  (map-tree! proc (cdr x)))
	(set-node-content! nx (proc x)))
    nx))

(define (compare nx ny)
  ""
  (define (create-reduxtab m)
    ;; Each `x' point (through `mx') to a list `ys' of elements `y' who
    ;; themselves point (through `my') to lists of things equal to `x'.
    ;; We unionize these, sort them, and, bind them as "things equal to `x'".
    (let* ((mx (first m))
	   (my (second m))
	   (mxf (third m))
	   (φ (lambda (x)
		(assoc-ref mx x)))
	   (φ* (lambda (y)
		 (assoc-ref my y)))
	   (Φ* (lambda (ys)
		 (apply lset-union eq? (map φ* ys)))))
      (append
       (lset-difference
	(lambda (x y) (eq? (car x) (car y)))
	mxf mx)
       (map
	(match-lambda
	  ((x . ys)
	   (cons
	    x
	    (let ((new-x (first (sort-list (Φ* ys) <)))
		  (fy (assoc-ref mxf x)))
	      (if (and (zo? new-x) fy)
		  (type-error 1 new-x fy)
		  (if (not fy)
		      new-x
		      (node-content
		       (map-tree!
			(lambda (y)
			  (first
			   (sort-list
			    (or (φ* y)
				(list (next)))
			    <)))
			(copy-tree (make-node fy))))))))))
	mx))))
  (define (massoc m x y)
    (define (assoc! m x y)
      (assoc-set! m x (cons y (or (assoc-ref m x) '()))))
    (list (if (and (not (pair? x))
		   (not (pair? y)))
	      (assoc! (first m) x y)
	      (first m))
	  (if (and (not (pair? x))
		   (not (pair? y)))
	      (assoc! (second m) y x)
	      (second m))
	  (if (not (pair? y))
	      (third m)
	      (if (assoc-ref (third m) x)
		  (type-error 0 "type variable already a function")
		  (assoc-set! (third m) x y)))))
  (create-reduxtab
   (let compare% ((m `(()
		       ;; 0_x ≡ 0_y (though we only use 0y→0x).
		       ;; Also, associations in an alist need to be mutable.
		       (,(cons 0 '(0)))
		       ()))
		  (nx nx)
		  (ny ny))
     (let ((x (node-content nx))
	   (y (node-content ny)))
       (cond
	;; same same
	((eq? nx ny)
	 m)
	((and (not x) (not y))
	 m)
	((and (zo? x) (zo? y))
	 m)
	((and (tv? x) (tv? y))
	 (massoc m x y))
	((and (pair? x) (pair? y))
	 (compare% (compare% m (car x) (car y))
		   (cdr x) (cdr y)))
	;; cases with #f
	((and (zo? x) (not y))
	 (set-node-content! ny x)
	 m)
	((and (zo? y) (not x))
	 (set-node-content! nx y)
	 m)
	((and (tv? y) (not x))
	 (let ((z (next)))
	   (set-node-content! nx z)
	   (massoc m y z)))
	((and (tv? x) (not y))
	 (let ((z (next)))
	   (set-node-content! ny z)
	   (massoc m x z)))
	((and (pair? x) (not y))
	 (set-node-content! ny x)
	 m)
	((and (pair? y) (not x))
	 (type-error 0 nx ny))
	;; cases with TVs
	((and (tv? x) (zo? y))
	 (massoc m x y))
	((and (tv? y) (zo? x))
	 (massoc m x y))
	((and (tv? x) (pair? y))
	 (massoc m x y))
	((and (tv? y) (pair? x))
	 (type-error 0 nx ny))
	;; outright incompatible cases
	((or (and (zo? x) (pair? y))
	     (and (zo? y) (pair? x)))
	 (type-error 1 x y))
	;; bad input
	(else
	 (type-error 2 nx ny)))))))

(define (reduce-tvs! reduxtab node)
  (map-tree!
   (lambda (x)
     (if (number? x)
	 (or (assoc-ref reduxtab x)
	     x)
	 x))
   node))

(define (rename-tvs! node)
  (let ((mapping (cons '() #f)))
    (map-tree!
     (lambda (x)
       (if (tv? x)
	   (if-let (z (assoc-ref (car mapping) x))
		   z
		   (let ((z (next)))
		     (set-car! mapping (assoc-set! (car mapping) x z))
		     z))
	   x))
     node)))

(define (apply-1 nx ny)
  (let ((x (node-content nx))
	(y (node-content ny)))
    (cond
     ((not x)
      (set-node-content! nx (cons ny (make-node #f)))
      (cdr (node-content nx)))
     ((zo? x)
      (type-error 3 x y))
     ((tv? x)
      (type-error 2 nx ny)
      )
     ((pair? x)
      (reduce-tvs! (compare (car x) ny)
		   (copy-tree (cdr x))))
     (else
      (type-error 2 nx ny)))))

(define (var-list? lst)
  "Predicate for valid macro variable lists."
  (and (every symbol? lst)
       (equal? lst (delete-duplicates lst))))

(define (sym-set! symtab name value)
  "Associate a type to a symbol in a symtab."
  (let ((m (module-name (current-module))))
    (assoc-set! symtab m
		(assoc-set! (or (assoc-ref symtab m) '())
			    name value))))

(define (sym-set symtab sym value)
  "Like `sym-set!' but returns a copy of the symtab."
  (sym-set! (alist-copy symtab) sym value))

(define (sym-ref symtab name)
  "Reference a symbol in a symtab returning its type."
  (let ((m (module-name (current-module))))
    (assoc-ref (or (assoc-ref symtab m) '()) name)))

(define (populate-tvs! node)
  "Add (in-place) new type variables to empty nodes of a tree."
  (map-tree!
   (lambda (x)
     (if (not x)
	 (next)
	 x))
   node))

(define (expand symtab expr)
  "Type check an expression returning two values: its resulting type and an
expansion where all applications of symbols present in the symtab are curried.
Raise a type error if the expression is invalid."
  (match expr
    (('quote x)
     (values
      (make-node 0)
      `',x))
    (('λc (? symbol? var) body)
     (let ((var-node (make-node #f)))
       (let ((bodyt bodye (expand (sym-set symtab var var-node)
				  body)))
	 (unless bodyt
	   (type-error 5 "in body of" `(λc ,var ,body)))
	 (populate-tvs! var-node)
	 (values
	  (make-node
	   (cons var-node bodyt))
	  `(lambda (,var) ,bodye)))))
    (('letrecc1 ((? symbol? name) expr) body)
     (type-error 0 'letrecc1 name expr body))
    (('cudefine name-vars body ..1)
     (values
      #f
      `(cudefine ,name-vars ,@body)))
    (('definec (? (lambda (x)
		    (and
		     (pair? x)
		     (symbol? (car x))
		     (var-list? (cdr x))))
		  (name vars ..1))
       body)
     (expand
      symtab
      `(definec ,name
	 ,(fold-right (lambda (x prev)
			`(λc ,x ,prev))
		      body vars))))
    (('definec (? symbol? name) body)
     (let ((t e (expand symtab body)))
       (if (not t)
	   (type-error 5 "in body of" `(definec ,name ,body))
	   ;; We need to declare the type twice. Once, in `expand', to ensure
	   ;; the type will be available in future `expand' calls. Second, in
	   ;; the expanded expression, so that it gets compiled.
	   (begin
	     (∷% name t)
	     (values
	      #f
	      `(begin
		 ((@@ (vouivre curry) ∷%)
		  ',name
		  ((@ (vouivre curry) parse)
		   ',(bare-type t)))
		 (define ,name ,e)))))))
    ((f)
     (let ((t e (expand symtab f)))
       (values
	(if t
	    (type-error 3 expr)
	    #f)
	`(,e))))
    ((f as ..1)
     (let ((ft fe (expand symtab f))
	   (ats aes (unzip2
		     (map
		      (lambda (a)
			(receive vals (expand symtab a) vals))
		      as))))
       (if (not ft)
	   (values #f `(,fe ,@aes))
	   (values
	    (fold
	     (lambda (at a prev)
	       (apply-1 prev (or at (type-error 5 f a))))
	     ft ats as)
	    (fold
	     (lambda (ae prev)
	       (list prev ae))
	     fe aes)))))
    (x
     (values
      (if (symbol? x)
	  (sym-ref symtab x)
	  (make-node 0))
      x))))

(define (bare-type x)
  "Return a type as tree s-expression without nodes."
  (and=>
   x
   (lambda (node)
     (let ((x (node-content node)))
       (cond
	((pair? x)
	 (cons (bare-type (car x)) (bare-type (cdr x))))
	(else x))))))

(define* (pt node #:optional (port current-output-port))
  "Print a tree to the given port in a cons cell format with '?' for empty
nodes."
  (format
   port "~a~%"
   (let pt% ((nx node))
     (let ((x (node-content nx)))
       (cond
	((not x)
	 "?")
	((number? x)
	 (number->string x))
	((pair? x)
	 (format #f "(~a . ~a)"
		 (pt% (car x))
		 (pt% (cdr x)))))))))

(define (parse x)
  "Parse a type from its cons cells representation to a tree."
  (if (not x)
      #f
      (second
       (let parse% ((tvs '())
		    (x x))
	 (cond
	  ((eq? x '?)
	   (list tvs (make-node #f)))
	  ((zo? x)
	   (list tvs (make-node 0)))
	  ((tv? x)
	   (if-let (z (assoc-ref tvs x))
		   (list tvs (make-node z))
		   (let ((tv (next)))
		     (list (assoc-set! tvs x tv)
			   (make-node tv)))))
	  ((pair? x)
	   (let* ((a (parse% tvs (car x)))
		  (b (parse% (first a) (cdr x))))
	     (list (first b) (make-node (cons (second a) (second b)))))))))))

(define (equal-types? ta tb)
  "Check the equality of two types."
  (define (equal-types?% ta tb correspondances)
    (let ((a (node-content ta))
	  (b (node-content tb)))
      (or
       (eq? a b)
       (let ((correspond?
	      (lambda (tx ty correspondances)
		(let ((x (node-content tx))
		      (y (node-content ty)))
		  (cond
		   ((and (zo? x)
			 (zo? y))
		    correspondances)
		   ((and (tv? x)
			 (tv? y))
		    (let ((xy (assoc-ref (car correspondances) x))
			  (yx (assoc-ref (cdr correspondances) y)))
		      (if (and (not xy) (not yx))
			 (cons (assoc-set! (car correspondances) x y)
			       (assoc-set! (cdr correspondances) y x))
			 (and (eq? xy y) (eq? yx x) correspondances))))
		   ((and (pair? x) (pair? y))
		    (equal-types?% tx ty correspondances))
		   (else #f))))))
	 (match (list a b)
	   (((a1 . a2) (b1 . b2))
	    (and=>
	     (correspond? a1 b1 correspondances)
	     (lambda (correspondances)
	       (correspond? a2 b2 correspondances))))
	   (else #f))))))
  (if (or (not ta) (not tb))
      (eq? ta tb)
      (and (equal-types?% ta tb '(() . ()))
	   #t)))

(define-syntax curried-untyped-define
  (syntax-rules ()
    ((_ (name var) body ...)
     (cdefine (name var) body ...))
    ((_ (name var1 var2 ...) body ...)
     (curried-untyped-define ((name var1) var2 ...) body ...))))

(define (∷% name type)
  (set! symtab (sym-set! symtab name type)))

(define-syntax-rule (∷ name type)
  (∷% 'name (parse 'type)))

(define* (type-of x #:optional port)
  "Print the type of a declared symbol to the given port."
  (if-let (x (expand symtab x))
	  (pt x port)
	  (format port "#f~%")))
