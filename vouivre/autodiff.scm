;;;; Copyright (C) 2023 Vouivre Digital Corporation
;;;;
;;;; This file is part of Vouivre.
;;;;
;;;; Vouivre is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.
;;;;
;;;; Vouivre is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public
;;;; License along with Vouivre. If not, see <https://www.gnu.org/licenses/>.

(define-module (vouivre autodiff)
  #:use-module (ice-9 receive)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (vouivre misc)
  #:use-module (vouivre promises)
  #:export
  (adot
   amap2
   differentiable-wrapper
   ewise1
   ewise2
   extend
   fdiff
   logsumexp
   make-batch
   maximum
   mean
   rdiff
   relu
   sum)
  #:replace
  ((i:* . *)
   (i:+ . +)
   (i:- . -)
   (i:/ . /)
   (i:abs . abs)
   (i:array-cell-ref . array-cell-ref)
   (i:array-ref . array-ref)
   (i:cos . cos)
   (i:exp . exp)
   (i:expt . expt)
   (i:fold . fold)
   (i:identity . identity)
   (i:log . log)
   (i:max . max)
   (i:min . min)
   (i:reduce . reduce)
   (i:sin . sin)
   (i:sqrt . sqrt)
   (i:tan . tan)))

;;;; array utilities

(define (rel->abs indices dimensions)
  (let rec ((s 0)
	    (p 1)
	    (is (reverse indices))
	    (ds (reverse dimensions)))
    (if (null? is)
	s
	(rec (+ s (* p (car is)))
	     (* p (car ds))
	     (cdr is)
	     (cdr ds)))))

(define(do-times n proc)
  (let rec ((i 0))
    (unless (= i n)
      (proc i)
      (rec (1+ i)))))

(define (contract-arrays a b n)
  (let* ((dims-a (array-dimensions a))
	 (dims-b (array-dimensions b))
	 (free-dims-a (take dims-a (- (array-rank a) n)))
	 (free-dims-b (drop dims-b n))
	 (bound-dims (take dims-b n))
	 (n-free-dims-a (apply * free-dims-a))
	 (n-free-dims-b (apply * free-dims-b))
	 (n-bound-dims (apply * bound-dims))
	 (s 0)
	 (r (apply make-array *unspecified*
		   (append free-dims-a free-dims-b)))
	 (ac (array-contents a))
	 (bc (array-contents b))
	 (rc (array-contents r)))
    (do-times
     n-free-dims-a
     (lambda (i)
       (let ((i-k (* n-bound-dims i))
	     (i-j (* n-free-dims-b i)))
	 (do-times
	  n-free-dims-b
	  (lambda (j)
	    (set! s 0)
	    (do-times
	     n-bound-dims
	     (lambda (k)
	       (set! s (+ s (* (array-ref ac (+ i-k k))
			       (array-ref bc (+ (* n-free-dims-b k) j)))))))
	    (array-set! rc s (+ i-j j)))))))
    r))

;;;; utilities that work on both numbers and arrays

(define (extend f)
  "Extend a function of one or more scalars to apply to numbers/arrays
element-wise. All arrays must have the same dimension."
  (define (apply-elemwise f indices args)
    (apply f (map (lambda (x)
		    (if (number? x)
			x
			(apply array-ref x indices)))
		  args)))
  (lambda xs
    (if-let (x (find array? xs))
	    (apply
	     produce-array
	     (lambda is
	       (apply-elemwise f is xs))
	     (array-dimensions x))
	    (apply f xs))))

;;;; differentiation

(define-record-type internal
  (make-internal forward jacobian)
  internal?
  (forward internal-forward)
  (jacobian internal-jacobian))

(define *differentiation-mode* (make-parameter #f))
(define *n-y-dims* (make-parameter #f))
(define *j* (make-parameter #f))

(define-syntax-rule (w/j val body ...)
  (parameterize ((*j* val))
    body ...))

(define (wrap axis)
  (lambda (x i)
    (if (= i axis)
	(make-internal x 'input)
	x)))

(define (unwrap-fwd x)
  (if (internal? x)
      (internal-forward x)
      x))

(define (unwrap-jac x)
  (if (internal? x)
      (internal-jacobian x)
      x))

(define (dims-of x)
  (if (number? x)
      '()
      (array-dimensions x)))

(define (add dst-buf src-buf n-dims)
  (do-times
   n-dims
   (lambda (i)
     (array-set!
      dst-buf
      (+ (array-ref dst-buf i)
	 (array-ref src-buf i))
      i)))
  dst-buf)

(define (movg dst-buf n-dst-dims generator naked-inputs data j)
  (do-times
   n-dst-dims
   (lambda (i)
     (array-set!
      dst-buf
      (apply generator naked-inputs i j data)
      i)))
  dst-buf)

(define (addg dst-buf n-dst-dims generator naked-inputs data j)
  (do-times
   n-dst-dims
   (lambda (i)
     (array-set!
      dst-buf
      (+ (array-ref dst-buf i)
	 (apply generator naked-inputs i j data))
      i)))
  dst-buf)

(define (movc dst-buf n-dst-dims src-buf n-src-dims
	      generator naked-inputs data)
  "Contract the Jacobian column produced by the generator with the source buffer
storing the result in the destination buffer."
  (let ((s 0))
    (do-times
     n-dst-dims
     (lambda (i)
       (set! s 0)
       (do-times
	n-src-dims
	(lambda (k)
	  (set! s (+ s (* (apply generator naked-inputs i k data)
			  (array-ref src-buf k))))))
       (array-set! dst-buf s i))))
  dst-buf)

(define (addc dst-buf n-dst-dims src-buf n-src-dims
	      generator naked-inputs data)
  "Contract the Jacobian column produced by the generator with the source buffer
adding the result to the destination buffer."
  (let ((s 0))
    (do-times
     n-dst-dims
     (lambda (i)
       (set! s (array-ref dst-buf i))
       (do-times
	n-src-dims
	(lambda (k)
	  (set! s (+ s (* (apply generator naked-inputs i k data)
			  (array-ref src-buf k))))))
       (array-set! dst-buf s i))))
  dst-buf)

(define (transpose-generator generator)
  (lambda (xs i j . data)
    (apply generator xs j i data)))

(define* (fdiff f #:optional (axis 0))
    "Automatic differentiation in forward accumulation mode w.r.t. the
function argument at the given index."
  (lambda xs
    (parameterize (((@@ (vouivre autodiff) *differentiation-mode*) 'fwd)
		   ((@@ (vouivre autodiff) *promises*) (cons '() #f)))
      (let* ((internal (apply f (map-indexed (wrap axis) xs)))
	     (fx (internal-forward internal))
	     (y (list-ref xs axis))  ; variable to differentiate w.r.t
	     (pre-Jx (internal-jacobian internal))
	     (Jx (cond
		  ;; TODO: implement 'input case and test 'zero and 'input
		  ((eq? pre-Jx 'zero)
		   (lambda (j)
		     (lambda (i)
		       0)))
		  ((eq? pre-Jx 'input)
		   (error "TBD."))
		  (else
		   (lambda (j)
		     (reset-promises (car (*promises*)))
		     (let ((column-jac (w/j j (force pre-Jx))))
		       (lambda (i)
			 (array-ref column-jac i))))))))
	(cond
	 ((and (number? fx) (number? y))
	  ((Jx 0) 0))
	 ((and (number? fx) (array? y))
	  (let* ((y-dims (array-dimensions y))
		 (a (apply make-array *unspecified* y-dims))
		 (ac (array-contents a)))
	    (do-times
	     (apply * y-dims)
	     (lambda (j)
	       (array-set! ac ((Jx j) 0)
			   j)))
	    a))
	 ((and (array? fx) (number? y))
	  (let* ((fx-dims (array-dimensions fx))
		 (a (apply make-array *unspecified* fx-dims))
		 (ac (array-contents a))
		 (Jx (Jx 0)))
	    (do-times
	     (apply * fx-dims)
	     (lambda (i)
	       (array-set! ac (Jx i)
			   i)))
	    a))
	 (else
	  (let* ((fx-dims (array-dimensions fx))
		 (y-dims (array-dimensions y))
		 (n-fx-dims (apply * fx-dims))
		 (n-y-dims (apply * y-dims))
		 (a (apply make-array *unspecified* (append fx-dims y-dims)))
		 (ac (array-contents a)))
	    (do-times
	     n-y-dims
	     (lambda (j)
	       (let ((Jx (Jx j)))
		 (do-times
		  n-fx-dims
		  (lambda (i)
		    (array-set! ac (Jx i)
				(+ j (* n-y-dims i))))))))
	    a)))))))

(define* (rdiff f #:optional (axis 0))
    "Automatic differentiation in reverse accumulation mode w.r.t. the
function argument at the given index."
  (lambda xs
    (parameterize (((@@ (vouivre autodiff) *differentiation-mode*) 'rev)
		   ((@@ (vouivre autodiff) *promises*) (cons '() #f)))
      (let* ((internal (apply f (map-indexed (wrap axis) xs)))
	     (fx (internal-forward internal))
	     (y (list-ref xs axis))  ; variable to differentiate w.r.t
	     (y-dims (dims-of y))
	     (pre-Jx (internal-jacobian internal))
	     (Jx (cond
		  ;; TODO: implement 'input case and test 'zero and 'input
		  ((eq? pre-Jx 'zero)
		   (lambda (i)
		     (lambda (j)
		       0)))
		  ((eq? pre-Jx 'input)
		   (error "TBD."))
		  (else
		   (let ((pre-Jx (pre-Jx #f)))
		     (lambda (i)
		       (let ((row-jac (pre-Jx i)))
			 (lambda (j)
			   (array-ref row-jac j)))))))))
	(parameterize ((*n-y-dims* (apply * y-dims)))
	  (cond
	   ((and (number? fx) (number? y))
	    ((Jx 0) 0))
	   ((and (number? fx) (array? y))
	    (let* ((a (apply make-array *unspecified* y-dims))
		   (ac (array-contents a))
		   (Jx (Jx 0)))
	      (do-times
	       (*n-y-dims*)
	       (lambda (j)
		 (array-set! ac (Jx j)
			     j)))
	      a))
	   ((and (array? fx) (number? y))
	    (let* ((fx-dims (array-dimensions fx))
		   (a (apply make-array *unspecified* fx-dims))
		   (ac (array-contents a)))
	      (do-times
	       (apply * fx-dims)
	       (lambda (i)
		 (array-set! ac ((Jx i) 0)
			     i)))
	      a))
	   (else
	    (let* ((fx-dims (array-dimensions fx))
		   (n-fx-dims (apply * fx-dims))
		   (a (apply make-array *unspecified* (append fx-dims y-dims)))
		   (ac (array-contents a)))
	      (do-times
	       n-fx-dims
	       (lambda (i)
		 (let ((Jx (Jx i)))
		   (do-times
		    (*n-y-dims*)
		    (lambda (j)
		      (array-set! ac (Jx j)
				  (+ j (* (*n-y-dims*) i))))))))
	      a))))))))

;; In the comment that follows:
;;
;; `n' is the number of arguments to `proc'.
;; `generators is not a `Vec' but a `List' we only use the former to illustrate
;; its length.
;; `X1', ..., `Xn' are the types of inputs and thus `Array's of some dimension.
;; `I' is the type of multi-indices indexing the output of `function'.
;; `J' is the type of multi-indices indexing the input array being differentiated.
;; `|I|' (resp. `|J|') is the type of absolute indices of `I' (resp. `J').
;; `Array I' is the type of arrays indexed by multi-indices of `I'.
;; `[X]' means that `X' is boxed in an internal as when returned by
;;       `differentiable-wrapper' with the array being `X' and the promise that
;;       given a |J| we will get the change of `X' with a change of the
;;       the differentiated argument at multi-index `J'.
;; (∷ (→ (Vec n (→ X1 ... Xn |I| |J| Number))
;;       (→ X1 ... Xn (Array I))*
;;       [X1] ... [Xn]
;;       (Internal (Array I) (Promise |J| (Array |I|)))))
;;
;; (*) We extend this definition to allow `proc' to be a list of procedures
;;     the head of which is as described above and the remaining elements
;;     are procedures of the same arguments but returning values that are
;;     then fed as extra data to the generators.
(define (differentiable-wrapper generators proc* arg . more)
  "Helper for defining differentiable functions.

generators: A list of generating procedures that, take the given arguments
            along with an absolute input index, an absolute output index,
            and zero or more data points, to produce a corresponding jacobian
            element.

proc*:      A list containing the function to be differentiated and zero or
            more procedures that also take the given arguments to create,
            in the forward pass, the data points that are latter fed to the
            generators.

arg . more: The arguments to all these procedures.

NOTE: In cases where an argument isn't meant to be differentiated against, its
      corresponding generator should be `#f'."
  (define (precompute-data naked-args)
    (if (procedure? proc*)
	'()
	(map (lambda (g)
	       (apply g naked-args))
	     (cdr proc*))))
  (let* ((args (cons arg more))
	 (proc (if (procedure? proc*)
		   proc*
		   (car proc*)))
	 (naked-args (map unwrap-fwd args))
	 (out (apply proc naked-args)))
    (case (*differentiation-mode*)
      ((#f)
       out)
      ((fwd)
       (let* ((data (precompute-data naked-args))
	      (n-out-dims (apply * (dims-of out)))
	      (buf (make-array *unspecified* n-out-dims)))
	 (make-internal
	  out
	  (fold
	   (lambda (generator arg prev)
	     (if (or (not (internal? arg))
		     (eq? 'zero (internal-jacobian arg)))
		 prev
		 (let ((Jx (internal-jacobian arg))
		       (n-fwd-dims (apply * (dims-of (unwrap-fwd arg)))))
		   (if (eq? Jx 'input)
		       (if (eq? prev 'zero)
			   (delay
			     (movg buf n-out-dims
				   generator naked-args data (*j*)))
			   (delay
			     (addg (force prev) n-out-dims
				   generator naked-args data (*j*))))
		       (if (eq? prev 'zero)
			   (delay
			     (movc buf n-out-dims (force Jx) n-fwd-dims
				   generator naked-args data))
			   (delay
			     (addc (force prev) n-out-dims
				   (force Jx) n-fwd-dims
				   generator naked-args data)))))))
	   'zero generators args))))
      ((rev)
       (let ((data (precompute-data naked-args))
	     (n-out-dims (apply * (dims-of out))))
	 (make-internal
	  out
	  (fold
	   (lambda (generator arg prev)
	     (let ((generator (transpose-generator generator)))
	       (if (or (not (internal? arg))
		       (eq? 'zero (internal-jacobian arg)))
		   prev
		   (let* ((Jx (internal-jacobian arg))
			  (n-fwd-dims (apply * (dims-of (unwrap-fwd arg)))))
		     (if (eq? Jx 'input)
			 (if (eq? prev 'zero)
			     (lambda (buf?)
			       (let ((dst-buf (make-array *unspecified*
							  n-fwd-dims)))
				 (if buf?
				     (lambda (buf)
				       (movc dst-buf n-fwd-dims
					     buf n-out-dims
					     generator naked-args data))
				     (lambda (i)
				       (movg dst-buf n-fwd-dims
					     generator naked-args data
					     i)))))
			     (lambda (buf?)
			       (let ((prev (prev buf?)))
				 (if buf?
				     (lambda (buf)
				       (addc (prev buf) n-fwd-dims
					     buf n-out-dims
					     generator naked-args data))
				     (lambda (i)
				       (addg (prev i) n-fwd-dims
					     generator naked-args data
					     i))))))
			 (if (eq? prev 'zero)
			     (lambda (buf?)
			       (let ((Jx (Jx #t))
				     (dst-buf (make-array *unspecified*
							  n-fwd-dims)))
				 (if buf?
				     (lambda (buf)
				       (Jx
					(movc dst-buf n-fwd-dims buf
					      n-out-dims
					      generator naked-args data)))
				     (lambda (i)
				       (Jx
					(movg dst-buf n-fwd-dims
					      generator naked-args data
					      i))))))
			     (lambda (buf?)
			       (let ((prev (prev buf?))
				     (Jx (Jx #t))
				     (dst-buf (make-array *unspecified*
							  n-fwd-dims)))
				 (if buf?
				     (lambda (buf)
				       (add (prev buf)
					    (Jx
					     (movc dst-buf n-fwd-dims
						   buf n-out-dims
						   generator naked-args data))
					    (*n-y-dims*)))
				     (lambda (i)
				       (add (prev i)
					    (Jx
					     (movg dst-buf n-fwd-dims
						   generator naked-args data
						   i))
					    (*n-y-dims*))))))))))))
	   'zero generators args)))))))

(define (ewise1 f)
  "Helper for creating the jacobian generator of one argument functions created
with the `extend' procedure."
  (lambda (xs i j)
    (let ((x (car xs)))
      (if (number? x)
	  (f x)
	  (ifn (= i j)
	       0
	       (f (array-ref (array-contents x)
			     j)))))))

(define (ewise2 proc axis)
  "Helper for creating the jacobian generators of two argument functions created
with the `extend' procedure."
  (lambda (xs i j)
    (let ((x (car xs))
	  (y (cadr xs)))
      (cond
       ((and (number? x) (number? y))
	(proc x y))
       ((and (number? x) (array? y))
	(if (= axis 0)
	    (proc x (array-ref (array-contents y)
			       i))
	    (ifn (= i j)
		 0
		 (proc x (array-ref (array-contents y)
				    j)))))
       ((and (array? x) (number? y))
	(if (= axis 1)
	    (proc (array-ref (array-contents x)
			     i)
		  y)
	    (ifn (= i j)
		 0
		 (proc (array-ref (array-contents x)
				  j)
		       y))))
       (else
	(ifn (= i j)
	     0
	     (proc (array-ref (array-contents x)
			      j)
		   (array-ref (array-contents y)
			      j))))))))

(define (i:identity x)
  "Differentiable identity."
  (differentiable-wrapper
   (list (ewise1 (lambda _ 1)))
   identity
   x))

(define (i:sqrt x)
  "Differentiable square root."
  (differentiable-wrapper
   (list (ewise1 (lambda (x) (/ 1 2 (sqrt x)))))
   (extend sqrt)
   x))

(define (i:exp x)
  "Differentiable exponential."
  (differentiable-wrapper
   (list (ewise1 exp))
   (extend exp)
   x))

(define (i:expt x y)
  "Differentiable power."
  (differentiable-wrapper
   (list (ewise2 (lambda (x y) (* y (expt x (1- y)))) 0)
	 (ewise2 (lambda (x y) (* (expt x y) (log x))) 1))
   (extend expt)
   x y))

(define (i:log x)
  "Differentiable logarithm."
  (differentiable-wrapper
   (list (ewise1 (lambda (x) (/ x))))
   (extend log)
   x))

(define (i:sin x)
  "Differentiable sine."
  (differentiable-wrapper
   (list (ewise1 cos))
   (extend sin)
   x))

(define (i:cos x)
  "Differentiable cosine."
  (differentiable-wrapper
   (list (ewise1 (lambda (x) (- (sin x)))))
   (extend cos)
   x))

(define (i:tan x)
  "Differentiable tangent."
  (differentiable-wrapper
   (list (ewise1 (lambda (x) (/ (expt (cos x) 2)))))
   (extend tan)
   x))

(define (i:+ x y)
  "Differentiable element-wise addition."
  (differentiable-wrapper
   (list
    (ewise2 (lambda _ +1) 0)
    (ewise2 (lambda _ +1) 1))
   (extend +)
   x y))

(define (i:- x y)
  "Differentiable element-wise subtraction."
  (differentiable-wrapper
   (list
    (ewise2 (lambda _ +1) 0)
    (ewise2 (lambda _ -1) 1))
   (extend -)
   x y))

(define (i:* x y)
  "Differentiable element-wise multiplication."
  (differentiable-wrapper
   (list
    (ewise2 (lambda (x y) y) 0)
    (ewise2 (lambda (x y) x) 1))
   (extend *)
   x y))

(define (i:/ x y)
  "Differentiable element-wise division."
  (differentiable-wrapper
   (list
    (ewise2 (lambda (x y) (/ y)) 0)
    (ewise2 (lambda (x y) (- (/ x y y))) 1))
   (extend /)
   x y))

(define (i:max x y)
  "Differentiable element-wise maximum."
  (define (dmax x y)
    (cond
     ((> x y)
      1)
     ((= x y)
      1/2)
     (else
      0)))
  (differentiable-wrapper
   (list
    (ewise2 dmax 0)
    (ewise2 (flip dmax) 1))
   (extend max)
   x y))

(define (i:min x y)
  "Differentiable element-wise minimum."
  (define (dmin x y)
    (cond
     ((< x y)
      1)
     ((= x y)
      1/2)
     (else
      0)))
  (differentiable-wrapper
   (list
    (ewise2 dmin 0)
    (ewise2 (flip dmin) 1))
   (extend min)
   x y))

(define (i:abs x)
  "Differentiable absolute."
  (differentiable-wrapper
   (list (ewise1 (lambda (x)
		   (cond ((> x 0)
			  +1)
			 ((= x 0)
			  1/2)
			 ((< x 0)
			  -1)))))
   (extend abs)
   x))

(define (mean x)
  "Differentiable mean on arrays."
  (differentiable-wrapper
   (list
    (lambda (xs i j one-over-n)
      one-over-n))
   (let ((n 0))
     (list
      (lambda (x)
	(let ((sum 0))
	  (array-for-each
	   (lambda (x)
	     (set! sum (+ sum x))
	     (set! n (1+ n)))
	   x)
	  (/ sum n)))
      (lambda _ (/ n))))
   x))

(define (i:array-ref x . indices)
  "Differentiable `array-ref' w.r.t the first argument."
  (apply
   differentiable-wrapper
   (cons
    (lambda (xs i j abs-index)
      (if (= j abs-index)
	  1
	  0))
    (map not indices))
   (list
    array-ref
    (lambda (x . indices)
      (rel->abs indices (array-dimensions x))))
   x indices))

(define (i:array-cell-ref x . indices)
  "Differentiable `array-cell-ref' w.r.t the first argument."
  (apply
   differentiable-wrapper
   (cons
    (lambda (xs i j abs-index n-rst-dims)
      (receive (j-ref j-rst) (euclidean/ j n-rst-dims)
	(if (and (= j-ref abs-index)
		 (= j-rst i))
	    1
	    0)))
    (map not indices))
   (list
    array-cell-ref
    (lambda (x . indices)
      (rel->abs indices (take (array-dimensions x)
			      (length indices))))
    (lambda (x . indices)
      (apply * (drop (array-dimensions x)
		     (length indices)))))
   x indices))

(define (make-batch elem . more)
  "Differentiable function to batch one or more arrays together."
  (let ((batch-size (1+ (length more))))
    (apply
     differentiable-wrapper
     (list-tabulate
      batch-size
      (lambda (b)
	(lambda (xs i j n-rest-dims)
	  (receive (i-batch i-rest) (euclidean/ i n-rest-dims)
	    (if (and (= i-batch b)
		     (= i-rest j))
		1
		0
		)))))
     (list
      (lambda (elem . more)
	(let ((a (apply make-array *unspecified* batch-size (dims-of elem))))
	  (for-each
	   (lambda (x b)
	     (array-cell-set! a x b))
	   (cons elem more)
	   (list-tabulate batch-size identity))
	  a))
      (lambda (elem . more)
	(apply * (dims-of elem))))
     elem more)))

(define (maximum x)
  "Differentiable maximum on arrays."
  (differentiable-wrapper
   (list
    (lambda (xs i j max-index)
      (if (= j max-index)
	  1
	  0)))
   (let ((max-index 'TBD))
     (list
      (lambda (x)
	(let ((m (- (inf)))
	      (i 0))
	  (array-for-each
	   (lambda (x)
	     (when (< m x)
	       (set! m x)
	       (set! max-index i))
	     (set! i (1+ i)))
	   x)
	  m))
      (lambda _ max-index)))
   x))

(define (sum x)
  "Differentiable sum on arrays."
  (differentiable-wrapper
   (list (lambda _ 1))
   (lambda (x)
     (let ((sum 0))
       (array-for-each
	(lambda (x)
	  (set! sum (+ sum x)))
	x)
       sum))
   x))

(define (adot x y n)
  "Differentiable array dot product."
  (differentiable-wrapper
   (list
    (lambda (xs i j n-free-dims-y n-bound-dims)
      (receive (i-x i-y) (euclidean/ i n-free-dims-y)
	(receive (j-free j-bound) (euclidean/ j n-bound-dims)
	  (ifn (= i-x j-free)
	       0
	       (array-ref (array-contents (cadr xs))
			  (+ i-y (* n-free-dims-y j-bound)))))))
    (lambda (xs i j n-free-dims-y n-bound-dims)
      (receive (i-x i-y) (euclidean/ i n-free-dims-y)
	(receive (j-bound j-free) (euclidean/ j n-free-dims-y)
	  (ifn (= i-y j-free)
	       0
	       (array-ref (array-contents (car xs))
			  (+ j-bound (* n-bound-dims i-x)))))))
    #f)
   (list
    contract-arrays
    (lambda (x y n)
      (apply * (drop (array-dimensions y)
		     n)))
    (lambda (x y n)
      (apply * (take (array-dimensions y)
		     n))))
   x y n))

(define (amap2 f x y)
  "Differentiable functional mapping on corresponding rows of two arrays with
rank > 0."
  (apply make-batch
	 (list-tabulate (car (dims-of (unwrap-fwd x)))
			(lambda (b)
			  (f (i:array-cell-ref x b)
			     (i:array-cell-ref y b))))))

(define (relu x)
  "Differentiable rectified linear unit."
  (i:max 0 x))

(define (logsumexp x)
  "Differentiable RealSoftMax using the log-sum-exp trick."
  (let ((c (maximum x)))
    (i:+ c (i:log (sum (i:exp (i:- x c)))))))

(define (i:fold f init lst . more)
  "Differentiable fold of a differentiable function."
  (apply fold f init lst more))

(define (i:reduce f default lst)
  "Differentiable reduce of a differentiable function."
  (reduce f default lst))
