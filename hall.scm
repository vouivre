(hall-description
 (name "vouivre")
 (prefix "")
 (version "0.2.0")
 (author "Vouivre Digital Corporation")
 (email "admin@vouivredigital.com")
 (copyright (2023))
 (synopsis "")
 (description "")
 (home-page "https://vouivredigital.com")
 (license gpl3+)
 (dependencies `())
 (skip ())
 (features ((guix #f) (native-language-support #f) (licensing #f)))
 (files (libraries
         ((scheme-file "vouivre")
          (directory
           "language"
           ((directory
             "vouivre"
             ((scheme-file "decompile-tree-il")
              (scheme-file "spec")
              (scheme-file "compile-tree-il")))))
          (directory
           "vouivre"
           ((scheme-file "hconfig")
            (scheme-file "curry")
            (scheme-file "autodiff")
            (scheme-file "misc")
            (scheme-file "promises")
            (scheme-file "mnist")))))
        (tests ((directory
                 "tests"
                 ((scheme-file "curry") (scheme-file "autodiff")))))
        (programs ((directory "scripts" ())
		   (directory "examples" ((scheme-file "example")
					  (scheme-file "base")))))
        (documentation
         ((org-file "README")
          (symlink "README" "README.org")
          (text-file "HACKING")
          (text-file "COPYING")
	  (text-file "COPYING.LESSER")
          (directory
           "doc"
           ((info-file "vouivre")
            (info-file "version")
            (texi-file "version")
            (text-file ".dirstamp")
            (texi-file "vouivre")
            (text-file "stamp-vti")))
	  (text-file "LICENSE")
          (text-file "NEWS")
          (text-file "AUTHORS")
          (text-file "ChangeLog")))
        (infrastructure
         ((scheme-file "guix")
          (text-file ".gitignore")
          (scheme-file "hall")
          (directory
           "build-aux"
           ((tex-file "texinfo")
            (text-file "mdate-sh")
            (scheme-file "test-driver")
            (text-file "missing")
            (text-file "install-sh")))
          (autoconf-file "configure")
          (in-file "pre-inst-env")
          (automake-file "Makefile")))))
