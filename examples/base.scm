;;;; Copyright (C) 2023 Vouivre Digital Corporation
;;;;
;;;; This file is part of Vouivre.
;;;;
;;;; Vouivre is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.
;;;;
;;;; Vouivre is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public
;;;; License along with Vouivre. If not, see <https://www.gnu.org/licenses/>.

(define-module (vouivre base)
  #:use-module ((guile) #:select (1+) #:prefix guile:)
  #:use-module ((rnrs base) #:prefix rnrs:)
  #:use-module ((srfi srfi-1) #:prefix srfi-1:)
  #:use-module ((vouivre autodiff) #:prefix v:)
  #:use-module (vouivre curry)
  #:export
  (∘
   ⊙
   fdiff
   flip
   rdiff)
  #:replace
  (boolean?
   not
   symbol?
   symbol->string
   string->symbol
   char?
   char=?
   char<?
   char>?
   char<=?
   char>=?
   integer->char
   char->integer
   list?
   null?
   pair?
   cons
   car
   cdr
   caar
   cadr
   cdar
   cddr
   caaar
   cadar
   cdaar
   caddr
   cdadr
   cddar
   cdddr
   caaaar
   caaadr
   caadar
   cadaar
   cdaaar
   cddaar
   cdadar
   cdaadr
   cadadr
   caaddr
   caddar
   cadddr
   cdaddr
   cddadr
   cdddar
   cddddr
   number?
   string?
   procedure?
   eq?
   eqv?
   equal?
   symbol=?
   complex?
   real-part
   imag-part
   make-rectangular
   make-polar
   magnitude
   angle
   sqrt
   exp
   expt
   log
   sin
   cos
   tan
   asin
   acos
   atan
   real?
   rational?
   numerator
   denominator
   rationalize
   exact?
   inexact?
   integer?
   odd?
   even?
   gcd
   lcm
   exact-integer-sqrt
   =
   <
   >
   <=
   >=
   zero?
   positive?
   negative?
   length
   list-ref
   list-tail
   append
   reverse
   number->string
   string->number
   make-string
   list->string
   string->list
   string-length
   string-ref
   string-copy
   substring
   string=?
   string<?
   string>?
   string<=?
   string>=?
   string-append
   +
   -
   *
   /
   max
   min
   abs
   truncate
   floor
   ceiling
   round
   div
   mod
   real-valued?
   rational-valued?
   integer-valued?
   nan?
   infinite?
   finite?
   fold
   fold-right
   reduce
   reduce-right
   map
   1+
   identity
   amap2
   adot
   logsumexp
   maximum
   mean
   relu
   sum
   fdiff
   rdiff))

;; abbreviation
(define-syntax cudefine (identifier-syntax curried-untyped-define))

(∷ boolean? (0 . 0))
(cudefine (boolean? x) (rnrs:boolean? x))

(∷ not (0 . 0))
(cudefine (not x) (rnrs:not x))

(∷ symbol? (0 . 0))
(cudefine (symbol? x) (rnrs:symbol? x))

(∷ symbol->string (0 . 0))
(cudefine (symbol->string x) (rnrs:symbol->string x))

(∷ string->symbol (0 . 0))
(cudefine (string->symbol x) (rnrs:string->symbol x))

(∷ char? (0 . 0))
(cudefine (char? x) (rnrs:char? x))

(∷ char=? (0 . (0 . 0)))
(cudefine (char=? x y) (rnrs:char=? x y))

(∷ char<? (0 . (0 . 0)))
(cudefine (char<? x y) (rnrs:char<? x y))

(∷ char>? (0 . (0 . 0)))
(cudefine (char>? x y) (rnrs:char>? x y))

(∷ char<=? (0 . (0 . 0)))
(cudefine (char<=? x y) (rnrs:char<=? x y))

(∷ char>=? (0 . (0 . 0)))
(cudefine (char>=? x y) (rnrs:char>=? x y))

(∷ integer->char (0 . 0))
(cudefine (integer->char x) (rnrs:integer->char x))

(∷ char->integer (0 . 0))
(cudefine (char->integer x) (rnrs:char->integer x))

(∷ list? (0 . 0))
(cudefine (list? x) (rnrs:list? x))

(∷ null? (0 . 0))
(cudefine (null? x) (rnrs:null? x))

(∷ pair? (0 . 0))
(cudefine (pair? x) (rnrs:pair? x))

(∷ cons (0 . (0 . 0)))
(cudefine (cons x y) (rnrs:cons x y))

(∷ car (0 . 0))
(cudefine (car x) (rnrs:car x))

(∷ cdr (0 . 0))
(cudefine (cdr x) (rnrs:cdr x))

(∷ caar (0 . 0))
(cudefine (caar x) (rnrs:caar x))

(∷ cadr (0 . 0))
(cudefine (cadr x) (rnrs:cadr x))

(∷ cdar (0 . 0))
(cudefine (cdar x) (rnrs:cdar x))

(∷ cddr (0 . 0))
(cudefine (cddr x) (rnrs:cddr x))

(∷ caaar (0 . 0))
(cudefine (caaar x) (rnrs:caaar x))

(∷ caadr (0 . 0))
(cudefine (caadr x) (rnrs:caadr x))

(∷ cadar (0 . 0))
(cudefine (cadar x) (rnrs:cadar x))

(∷ cdaar (0 . 0))
(cudefine (cdaar x) (rnrs:cdaar x))

(∷ caddr (0 . 0))
(cudefine (caddr x) (rnrs:caddr x))

(∷ cdadr (0 . 0))
(cudefine (cdadr x) (rnrs:cdadr x))

(∷ cddar (0 . 0))
(cudefine (cddar x) (rnrs:cddar x))

(∷ cdddr (0 . 0))
(cudefine (cdddr x) (rnrs:cdddr x))

(∷ caaaar (0 . 0))
(cudefine (caaaar x) (rnrs:caaaar x))

(∷ caaadr (0 . 0))
(cudefine (caaadr x) (rnrs:caaadr x))

(∷ caadar (0 . 0))
(cudefine (caadar x) (rnrs:caadar x))

(∷ cadaar (0 . 0))
(cudefine (cadaar x) (rnrs:cadaar x))

(∷ cdaaar (0 . 0))
(cudefine (cdaaar x) (rnrs:cdaaar x))

(∷ cddaar (0 . 0))
(cudefine (cddaar x) (rnrs:cddaar x))

(∷ cdadar (0 . 0))
(cudefine (cdadar x) (rnrs:cdadar x))

(∷ cdaadr (0 . 0))
(cudefine (cdaadr x) (rnrs:cdaadr x))

(∷ cadadr (0 . 0))
(cudefine (cadadr x) (rnrs:cadadr x))

(∷ caaddr (0 . 0))
(cudefine (caaddr x) (rnrs:caaddr x))

(∷ caddar (0 . 0))
(cudefine (caddar x) (rnrs:caddar x))

(∷ cadddr (0 . 0))
(cudefine (cadddr x) (rnrs:cadddr x))

(∷ cdaddr (0 . 0))
(cudefine (cdaddr x) (rnrs:cdaddr x))

(∷ cddadr (0 . 0))
(cudefine (cddadr x) (rnrs:cddadr x))

(∷ cdddar (0 . 0))
(cudefine (cdddar x) (rnrs:cdddar x))

(∷ cddddr (0 . 0))
(cudefine (cddddr x) (rnrs:cddddr x))

(∷ number? (0 . 0))
(cudefine (number? x) (rnrs:number? x))

(∷ string? (0 . 0))
(cudefine (string? x) (rnrs:string? x))

(∷ procedure? (0 . 0))
(cudefine (procedure? x) (rnrs:procedure? x))

(∷ eq? (0 . (0 . 0)))
(cudefine (eq? x y) (rnrs:eq? x y))

(∷ eqv? (0 . (0 . 0)))
(cudefine (eqv? x y) (rnrs:eqv? x y))

(∷ equal? (0 . (0 . 0)))
(cudefine (equal? x y) (rnrs:equal? x y))

(∷ symbol=? (0 . (0 . 0)))
(cudefine (symbol=? x y) (rnrs:symbol=? x y))

(∷ complex? (0 . 0))
(cudefine (complex? x) (rnrs:complex? x))

(∷ real-part (0 . 0))
(cudefine (real-part x) (rnrs:real-part x))

(∷ imag-part (0 . 0))
(cudefine (imag-part x) (rnrs:imag-part x))

(∷ make-rectangular (0 . (0 . 0)))
(cudefine (make-rectangular x y) (rnrs:make-rectangular x y))

(∷ make-polar (0 . (0 . 0)))
(cudefine (make-polar x y) (rnrs:make-polar x y))

(∷ magnitude (0 . 0))
(cudefine (magnitude x) (rnrs:magnitude x))

(∷ angle (0 . 0))
(cudefine (angle x) (rnrs:angle x))

(∷ sqrt (0 . 0))
(cudefine (sqrt x) (v:sqrt x))

(∷ exp (0 . 0))
(cudefine (exp x) (v:exp x))

(∷ expt (0 . (0 . 0)))
(cudefine (expt x y) (v:expt x y))

(∷ log (0 . 0))
(cudefine (log x) (v:log x))

(∷ sin (0 . 0))
(cudefine (sin x) (v:sin x))

(∷ cos (0 . 0))
(cudefine (cos x) (v:cos x))

(∷ tan (0 . 0))
(cudefine (tan x) (v:tan x))

(∷ asin (0 . 0))
(cudefine (asin x) (rnrs:asin x))

(∷ acos (0 . 0))
(cudefine (acos x) (rnrs:acos x))

(∷ atan (0 . 0))
(cudefine (atan x) (rnrs:atan x))

(∷ real? (0 . 0))
(cudefine (real? x) (rnrs:real? x))

(∷ rational? (0 . 0))
(cudefine (rational? x) (rnrs:rational? x))

(∷ numerator (0 . 0))
(cudefine (numerator x) (rnrs:numerator x))

(∷ denominator (0 . 0))
(cudefine (denominator x) (rnrs:denominator x))

(∷ rationalize (0 . (0 . 0)))
(cudefine (rationalize x eps) (rnrs:rationalize x eps))

(∷ exact? (0 . 0))
(cudefine (exact? x) (rnrs:exact? x))

(∷ inexact? (0 . 0))
(cudefine (inexact? x) (rnrs:inexact? x))

(∷ integer? (0 . 0))
(cudefine (integer? x) (rnrs:integer? x))

(∷ odd? (0 . 0))
(cudefine (odd? x) (rnrs:odd? x))

(∷ even? (0 . 0))
(cudefine (even? x) (rnrs:even? x))

(∷ gcd (0 . (0 . 0)))
(cudefine (gcd x y) (rnrs:gcd x y))

(∷ lcm (0 . (0 . 0)))
(cudefine (lcm x y) (rnrs:lcm x y))

(∷ exact-integer-sqrt (0 . 0))
(cudefine (exact-integer-sqrt x) (rnrs:exact-integer-sqrt x))

(∷ = (0 . (0 . 0)))
(cudefine (= x y) (rnrs:= x y))

(∷ < (0 . (0 . 0)))
(cudefine (< x y) (rnrs:< x y))

(∷ > (0 . (0 . 0)))
(cudefine (> x y) (rnrs:> x y))

(∷ <= (0 . (0 . 0)))
(cudefine (<= x y) (rnrs:<= x y))

(∷ >= (0 . (0 . 0)))
(cudefine (>= x y) (rnrs:>= x y))

(∷ zero? (0 . 0))
(cudefine (zero? x) (rnrs:zero? x))

(∷ positive? (0 . 0))
(cudefine (positive? x) (rnrs:positive? x))

(∷ negative? (0 . 0))
(cudefine (negative? x) (rnrs:negative? x))

(∷ length (0 . 0))
(cudefine (length x) (rnrs:length x))

(∷ list-ref (0 . (0 . 0)))
(cudefine (list-ref lst k) (rnrs:list-ref lst k))

(∷ list-tail (0 . (0 . 0)))
(cudefine (list-tail lst k) (rnrs:list-tail lst k))

(∷ append (0 . (0 . 0)))
(cudefine (append x y) (rnrs:append x y))

(∷ reverse (0 . 0))
(cudefine (reverse x) (rnrs:reverse x))

(∷ number->string (0 . (0 . 0)))
(cudefine (number->string n radix) (rnrs:number->string n radix))

(∷ string->number (0 . (0 . 0)))
(cudefine (string->number str radix) (rnrs:string->number str radix))

(∷ make-string (0 . (0 . 0)))
(cudefine (make-string k char) (rnrs:make-string k char))

(∷ list->string (0 . 0))
(cudefine (list->string x) (rnrs:list->string x))

(∷ string->list (0 . (0 . (0 . 0))))
(cudefine (string->list str start end) (rnrs:string->list str start end))

(∷ string-length (0 . 0))
(cudefine (string-length x) (rnrs:string-length x))

(∷ string-ref (0 . (0 . 0)))
(cudefine (string-ref str k) (rnrs:string-ref str k))

(∷ string-copy (0 . (0 . (0 . 0))))
(cudefine (string-copy str start end) (rnrs:string-copy str start end))

(∷ substring (0 . (0 . (0 . 0))))
(cudefine (substring str start end) (rnrs:substring str start end))

(∷ string=? (0 . (0 . 0)))
(cudefine (string=? x y) (rnrs:string=? x y))

(∷ string<? (0 . (0 . 0)))
(cudefine (string<? x y) (rnrs:string<? x y))

(∷ string>? (0 . (0 . 0)))
(cudefine (string>? x y) (rnrs:string>? x y))

(∷ string<=? (0 . (0 . 0)))
(cudefine (string<=? x y) (rnrs:string<=? x y))

(∷ string>=? (0 . (0 . 0)))
(cudefine (string>=? x y) (rnrs:string>=? x y))

(∷ string-append (0 . (0 . 0)))
(cudefine (string-append x y) (rnrs:string-append x y))

(∷ + (0 . (0 . 0)))
(cudefine (+ x y) (v:+ x y))

(∷ - (0 . (0 . 0)))
(cudefine (- x y) (v:- x y))

(∷ * (0 . (0 . 0)))
(cudefine (* x y) (v:* x y))

(∷ / (0 . (0 . 0)))
(cudefine (/ x y) (v:/ x y))

(∷ max (0 . (0 . 0)))
(cudefine (max x y) (v:max x y))

(∷ min (0 . (0 . 0)))
(cudefine (min x y) (v:min x y))

(∷ abs (0 . 0))
(cudefine (abs x) (v:abs x))

(∷ truncate (0 . 0))
(cudefine (truncate x) (rnrs:truncate x))

(∷ floor (0 . 0))
(cudefine (floor x) (rnrs:floor x))

(∷ ceiling (0 . 0))
(cudefine (ceiling x) (rnrs:ceiling x))

(∷ round (0 . 0))
(cudefine (round x) (rnrs:round x))

(∷ div (0 . (0 . 0)))
(cudefine (div x y) (rnrs:div x y))

(∷ mod (0 . (0 . 0)))
(cudefine (mod x y) (rnrs:mod x y))

(∷ real-valued? (0 . 0))
(cudefine (real-valued? x) (rnrs:real-valued? x))

(∷ rational-valued? (0 . 0))
(cudefine (rational-valued? x) (rnrs:rational-valued? x))

(∷ integer-valued? (0 . 0))
(cudefine (integer-valued? x) (rnrs:integer-valued? x))

(∷ nan? (0 . 0))
(cudefine (nan? x) (rnrs:nan? x))

(∷ infinite? (0 . 0))
(cudefine (infinite? x) (rnrs:infinite? x))

(∷ finite? (0 . 0))
(cudefine (finite? x) (rnrs:finite? x))

(∷ fold ((0 . (0 . 0)) . (0 . (0 . 0))))
(cudefine (fold f x xs)
	  (srfi-1:fold
	   (lambda (x prev)
	     ((f x) prev))
	   x xs))

(∷ fold-right ((0 . (0 . 0)) . (0 . (0 . 0))))
(cudefine (fold-right f x xs)
	  (srfi-1:fold-right
	   (lambda (x prev)
	     ((f x) prev))
	   x xs))

(∷ reduce ((0 . (0 . 0)) . (0 . 0)))
(cudefine (reduce f xs)
	  (when (null? xs)
	    (error "empty list"))
	  (srfi-1:reduce
	   (lambda (x prev)
	     ((f x) prev))
	   0
	   xs))

(∷ reduce-right ((0 . (0 . 0)) . (0 . 0)))
(cudefine (reduce-right f xs)
	  (when (null? xs)
	    (error "empty list"))
	  (srfi-1:reduce-right
	   (lambda (x prev)
	     ((f x) prev))
	   0
	   xs))

(∷ map ((0 . 0) . (0 . 0)))
(cudefine (map f xs) (srfi-1:map f xs))

(∷ 1+ (0 . 0))
(cudefine (1+ x) (guile:1+ x))

(∷ adot (0 . (0 . (0 . 0))))
(cudefine (adot x y n) (v:adot x y n))

(∷ amap2 ((0 . (0 . 0)) . (0 . (0 . 0))))
(cudefine (amap2 f x y) (v:amap2 f x y))

(∷ logsumexp (0 . 0))
(cudefine (logsumexp x) (v:logsumexp x))

(∷ maximum (0 . 0))
(cudefine (maximum x) (v:maximum x))

(∷ mean (0 . 0))
(cudefine (mean x) (v:mean x))

(∷ relu (0 . 0))
(cudefine (relu x) (v:relu x))

(∷ sum (0 . 0))
(cudefine (sum x) (v:sum x))

(∷ fdiff ((0 . 0) . (0 . 0)))
(cudefine (fdiff x) (v:fdiff x))

(∷ rdiff ((0 . 0) . (0 . 0)))
(cudefine (rdiff x) (v:rdiff x))

(definec (identity x) x)
(definec (∘ g f) (λc x (g (f x))))
(definec (⊙ f g) (∘ g f))
(definec (flip f) (λc y (λc x (f x y))))
