;;;; Copyright (C) 2023 Vouivre Digital Corporation
;;;;
;;;; This file is part of Vouivre.
;;;;
;;;; Vouivre is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.
;;;;
;;;; Vouivre is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public
;;;; License along with Vouivre. If not, see <https://www.gnu.org/licenses/>.

(define-module (vouivre autodiff tests)
  #:use-module ((vouivre autodiff) #:prefix v:)
  #:use-module (ice-9 receive)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-64)
  #:use-module (vouivre misc)
  #:export
  (apply-diff
   a~
   const-generator
   differentiable-func-generator
   lambda-const-call
   ndiff
   n~
   random-array
   random-array-shape
   random-func1
   random-func2
   random-func2-rank&dims>0
   random-input
   random-list-element
   random-non-empty-array
   random-shape
   random-shared
   random-shared-array-rank&dims>0
   random-shared-contractible
   with-generators
   ~))

(define f1s (list v:abs v:cos v:exp v:identity v:sin))
(define f2s (list v:+ v:- v:* v:max v:min))

(define (with-generators% generators equal proc1 proc2 . more)
  "Check that all procedures return the same value according to `equal' when
evaluated on arguments produced by the generators (the number of generators
being the number of arguments to each procedure."
  (let ((times 100)
	(procs (cons proc1 (cons proc2 more))))
    (call/cc
     (lambda (break)
       (do ((i 0 (1+ i)))
	   ((= i times) #t)
	 (let ((zs (map-in-order (lambda (g) (g)) generators)))
	   (with-exception-handler
	       (lambda (e)
		 (break #f zs))
	     (lambda ()
	       (let* ((rs (map (lambda (f) (apply f zs)) procs))
		      (head (car rs)))
		 (unless (every (lambda (x) (equal x head))
				(cdr rs))
		   (break #f zs rs))))
	     #:unwind? #t)))))))

(define-syntax-rule (with-generators (g1 g2 ...) equal expected given more ...)
  (with-generators% (list g1 g2 ...) equal expected given more ...))

(define (lambda-const-call f . consts)
  (lambda _
    (apply f consts)))

(define* (random-array-shape
	  #:optional (min-rank 0) (max-rank 5) (min-dim 0) (max-dim 5))
  (list-tabulate (+ min-rank (random (- max-rank min-rank)))
		 (lambda _ (+ min-dim (random (- max-dim min-dim))))))

(define (random-shape)
  (if (= 0 (random 2))
      0
      (random-array-shape)))

(define* (random-array #:optional shape)
  (apply produce-array
	 (lambda _ (random:uniform))
	 (or shape (random-array-shape))))

(define (random-non-empty-array)
  "Random array of at least one element."
  (random-array (random-array-shape 0 5 1 5)))

(define* (random-input #:optional shape)
  (let ((shape (or shape (random-shape))))
    (if (eq? 0 shape)
	(random:uniform)
	(random-array shape))))

(define (random-shared)
  (let ((shape (random-shape)))
    (values
     (lambda ()
       (random-input shape))
     (lambda ()
       (let ((x (random-input
		 (random-list-element
		  (list 0 (if (list? shape)
			      shape
			      (random-shape)))))))
	 (set! shape (random-shape))
	 x)))))

(define (random-shared-array-rank&dims>0)
  (let ((shape (random-array-shape 1 5 1 5)))
    (values
     (lambda ()
       (random-array shape))
     (lambda ()
       (let ((x (random-array shape)))
	 (set! shape (random-array-shape 1 5 1 5))
	 x)))))

(define (random-list-element lst)
  (list-ref lst (random (length lst))))

(define (const-generator generator)
  (lambda ()
    generator))

(define (differentiable-func-generator lst . input-generators)
  (lambda ()
    (random-list-element
     (cons
      (apply
       lambda-const-call
       (random-list-element lst)
       (map (lambda (g) (g))
	    input-generators))
      lst))))

(define random-func1
  (differentiable-func-generator f1s random-input))
(define random-func2
  (receive (gx gy) (random-shared)
    (differentiable-func-generator f2s gx gy)))

(define* (n~ x y #:optional (error 1e-4))
  (and
   (>= y (- x error))
   (<= y (+ x error))))

(define* (a~ x y #:optional (error 1e-4))
  (and
   (equal? (array-dimensions x)
	   (array-dimensions y))
   (call/cc
    (lambda (break)
      (array-for-each
       (lambda (x y)
	 (unless (~ x y error)
	   (break #f)))
       x y)
      #t))))

(define* (~ x y #:optional (error 1e-4))
  (cond
   ((and (number? x) (number? y))
    (n~ x y error))
   ((and (array? x) (array? y))
    (a~ x y error))
   (else #f)))

(define* (ndiff f #:optional (axis 0) (step 1e-6))
  "Differentiation using a numerical centered difference approximation."
  (define (axis-add xs dh . indices)
    "Add `dh' to the number or array at the given `axis' of `xs',
and, when it's an array, at the given index."
    (map-indexed
     (lambda (x i)
       (ifn (= i axis)
	    x
	    (if (number? x)
		(+ x dh)
		(array-map-indexed
		 (lambda (x . indices_)
		   (ifn (equal? indices indices_)
			x
			(+ x dh)))
		 x))))
     xs))
  (lambda xs
    ;; We need the output shape and the input shape along the
    ;; differentiated axis.
    (let ((fxs (apply f xs))
	  (x (list-ref xs axis)))
      (cond
       ((and (number? fxs)
	     (number? x))
	(/ (- (apply f (axis-add xs step))
	      (apply f (axis-add xs (- step))))
	   (* 2 step)))
       ((and (number? fxs)
	     (array? x))
	(apply
	 produce-array
	 (lambda indices
	   (/ (- (apply f (apply axis-add xs step indices))
		 (apply f (apply axis-add xs (- step) indices)))
	      (* 2 step)))
	 (array-dimensions x)))
       ((and (array? fxs)
	     (number? x))
	((v:extend /)
	 ((v:extend -)
	  (apply f (axis-add xs step))
	  (apply f (axis-add xs (- step))))
	 (* 2 step)))
       ((and (array? fxs)
	     (array? x))
	(let ((a (apply
		  make-array *unspecified*
		  (append (array-dimensions fxs)
			  (array-dimensions x)))))
	  (for-indices-in-range
	   (lambda indices-in
	     (let ((dfxs ((v:extend /)
			  ((v:extend -)
			   (apply f (apply axis-add xs step indices-in))
			   (apply f (apply axis-add xs (- step) indices-in)))
			  (* 2 step))))
	       (for-indices-in-range
		(lambda indices-out
		  (apply
		   array-set!
		   a
		   (apply array-ref dfxs indices-out)
		   (append indices-out indices-in)))
		(list-zeros (array-rank fxs))
		(array-dimensions fxs))))
	   (list-zeros (array-rank x))
	   (array-dimensions x))
	  a))))))

(define* (apply-diff differentiator #:optional (axis 0))
  "Apply a differentiator (`ndiff', `fdiff', `rdiff') to a function and its
arguments (this is a convenience function)."
  (lambda (f . args)
    (apply (differentiator f axis) args)))

(test-begin "autodiff")

;; not differentiating
(test-assert (with-generators (random-input) ~ (v:extend identity) v:identity))
(test-assert (with-generators (random-input) ~ (v:extend exp) v:exp))
(test-assert
    (receive (gx gy) (random-shared)
      (with-generators (gx gy) ~ (v:extend *) v:*)))

;; differentiation in one variable
(test-assert
    (with-generators
     (random-func1 random-input)
     ~ (apply-diff ndiff) (apply-diff v:fdiff) (apply-diff v:rdiff)))

;; `v:mean' only takes non-empty arrays so we treat it separately
(test-assert
    (with-generators
     ((differentiable-func-generator (list v:mean) random-non-empty-array)
      random-non-empty-array)
     ~ (apply-diff ndiff) (apply-diff v:fdiff) (apply-diff v:rdiff)))

;; differentiation in two variables
(test-assert
    (receive (gx gy) (random-shared)
      (with-generators
       (random-func2 gx gy)
       ~ (apply-diff ndiff 0) (apply-diff v:fdiff 0) (apply-diff v:rdiff 0))))
(test-assert
    (receive (gx gy) (random-shared)
      (with-generators
       (random-func2 gx gy)
       ~ (apply-diff ndiff 1) (apply-diff v:fdiff 1) (apply-diff v:rdiff 1))))

;; `v:amap2' only takes arrays of rank > 0 and batch-size > 0 so we treat it
;; separately
(define random-func2-rank&dims>0
  (receive (gx gy) (random-shared-array-rank&dims>0)
    (differentiable-func-generator f2s gx gy)))
(test-assert
    (receive (gx gy) (random-shared-array-rank&dims>0)
      (with-generators
       ((const-generator v:amap2) random-func2-rank&dims>0 gx gy)
       ;; NOTE: for `v:amap2' the differentiable axes are 1 and 2.
       ~ (apply-diff ndiff 1) (apply-diff v:fdiff 1) (apply-diff v:rdiff 1))))
(test-assert
    (receive (gx gy) (random-shared-array-rank&dims>0)
      (with-generators
       ((const-generator v:amap2) random-func2-rank&dims>0 gx gy)
       ~ (apply-diff ndiff 2) (apply-diff v:fdiff 2) (apply-diff v:rdiff 2))))
(let* ((z #(1 2 3))
       (f (lambda (a)
	    (v:amap2 (lambda (x y)
		       (v:* a a))
		     #(10 20 30)
		     #(40 50 60))))
       (e ((ndiff f) z)))
  (test-assert (~ e ((v:fdiff f) z)))
  (test-assert (~ e ((v:rdiff f) z))))

;; `v:adot'
(define (random-shared-contractible)
  "Returns three generators: the first two generate arrays that are contractible
according to the number generated by the third one."
  (let* ((n (random 5))
	 (sa (random-array-shape n))
	 (sb (append (reverse (take (reverse sa)
				    n))
		     (random-array-shape 0 (- 5 n)))))
    (values
     (lambda ()
       (random-array sa))
     (lambda ()
       (random-array sb))
     (lambda ()
       (let ((tmp n))
	 (set! n (random 5))
	 (set! sa (random-array-shape n))
	 (set! sb (append (reverse (take (reverse sa)
					 n))
			  (random-array-shape 0 (- 5 n))))
	 tmp)))))
(test-assert
    (receive (gx gy gz) (random-shared-contractible)
      (with-generators
       ((const-generator v:adot) gx gy gz)
       ~ (apply-diff ndiff 0) (apply-diff v:fdiff 0) (apply-diff v:rdiff 0))))
(test-assert
    (receive (gx gy gz) (random-shared-contractible)
      (with-generators
       ((const-generator v:adot) gx gy gz)
       ~ (apply-diff ndiff 1) (apply-diff v:fdiff 1) (apply-diff v:rdiff 1))))

;; let binding re-entry
(test-assert
    (with-generators
     ((const-generator
       (lambda (x)
	 (let ((c (v:maximum x)))
	   (v:+ c (v:- x c)))))
      random-non-empty-array)
     ~ (apply-diff ndiff) (apply-diff v:fdiff) (apply-diff v:rdiff)))

;; chain rule
(test-assert
    (with-generators
     (random-func1 random-func1 random-input)
     ~
     (lambda (f g x) ((ndiff (compose f g)) x))
     (lambda (f g x) ((v:fdiff (compose f g)) x))
     (lambda (f g x) ((v:rdiff (compose f g)) x))))

(test-end "autodiff")
