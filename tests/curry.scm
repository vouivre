;;;; Copyright (C) 2023 Vouivre Digital Corporation
;;;;
;;;; This file is part of Vouivre.
;;;;
;;;; Vouivre is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.
;;;;
;;;; Vouivre is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public
;;;; License along with Vouivre. If not, see <https://www.gnu.org/licenses/>.

(define-module (vouivre curry tests)
  #:use-module (vouivre curry)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-71))

(test-begin "curry")
;; TODO: map the reduxtab to a bare version for unit-testing
;; (test-assert
;;     (lset=
;;      'equal?
;;      '((1 . (2 . 4)) (2 . 2) (3 . 2))
;;      (compare
;;       (parse '(1       . (2 . 3)))
;;       (parse '((1 . 2) . (1 . 1))))))

(define-syntax-rule (test-type given expected-bare)
  (test-assert (equal-types? given (parse expected-bare))))

(define (sym-sets symtab alist)
  (fold (lambda (x prev)
	  ((@@ (vouivre curry) sym-set) prev (car x) (cdr x)))
	symtab alist))

(let ((t e (expand '() '(λc x x))))
  (test-type t '(1 . 1))
  (test-equal '(lambda (x) x) e))
(let ((t e (expand '() '(λc g (λc f (λc x (g (f x))))))))
  (test-type t '((3 . 4) . ((2 . 3) . (2 . 4))))
  (test-equal '(lambda (g) (lambda (f) (lambda (x) (g (f x))))) e))
(let ((t e (expand '() '((λc x x) #t))))
  (test-type t 0)
  (test-equal #t (primitive-eval e)))
(let ((bindings
       (sym-sets
	'()
	`((id  . ,(parse '(1 . 1)))
	  (∘   . ,(parse '((2 . 3) . ((1 . 2) . (1 . 3)))))
	  (⊙   . ,(parse '((1 . 2) . ((2 . 3) . (1 . 3)))))
	  (map . ,(parse '((0 . 0) . (0 . 0))))
	  (+   . ,(parse '(0 . (0 . 0))))))))
  (let ((t e (expand bindings '(∘ id id))))
    (test-type t '(7 . 7))
    (test-equal e '((∘ id) id)))
  (let ((t e (expand bindings '((∘ id id) #t))))
    (test-type t 0)
    (test-equal e '(((∘ id) id) #t)))
  (let ((t e (expand bindings '(∘ id id #t))))
    (test-type t 0)
    (test-equal e '(((∘ id) id) #t)))
  (let ((t e (expand bindings '(λc f (∘ f)))))
    (test-type t '((2 . 3) . ((1 . 2) . (1 . 3)))))
  (let ((t e (expand bindings '(map (+ 1) '(1 2 3)))))
    (test-type t 0)
    (test-equal e '((map (+ 1)) '(1 2 3))))
  (let ((t e (expand bindings '(∘ +))))
    (test-type t '((1 . 0) . (1 . (0 . 0))))
    (test-equal e '(∘ +)))
  (let ((t e (expand bindings '((∘ +) (+ 1) 2 3))))
    (test-type t 0)
    (test-equal e '((((∘ +) (+ 1)) 2) 3)))
  (let ((t e (expand bindings '((∘ + (+ 1)) 2 3))))
    (test-type t 0)
    (test-equal e '((((∘ +) (+ 1)) 2) 3)))
  (let ((t e (expand bindings '(((∘ + (+ 1)) 2) 3))))
    (test-type t 0)
    (test-equal e '((((∘ +) (+ 1)) 2) 3)))
  (let ((t e (expand bindings '((∘ (∘ (+ 1)) +) 2 3))))
    (test-type t 0)
    (test-equal e '((((∘ (∘ (+ 1))) +) 2) 3)))
  (let ((t e (expand bindings '((∘ (⊙ (+ 1)) +) 2 3))))
    (test-type t 0)
    (test-equal e '((((∘ (⊙ (+ 1))) +) 2) 3))))

;;; interaction between typed and untyped (regular) scheme

;; Untyped scheme produces untyped return.
(let ((t e (expand '() '(+ 1 2 3))))
  (test-type t #f)
  (test-equal e '(+ 1 2 3)))

(let ((bindings
       (sym-sets
	'()
	`((* . ,(parse '(0 . (0 . 0))))))))
  ;; Typed Scheme can be used by untyped Scheme...
  (let ((t e (expand bindings '(+ 1 (* 2 3) 4))))
    (test-type t #f)
    (test-equal e '(+ 1 ((* 2) 3) 4)))

  ;; ... although, sometimes, with terrible runtime consequences!
  (let ((t e (expand bindings '(+ 1 (* 2) 3))))
    (test-type t #f)
    (test-equal e '(+ 1 (* 2) 3)))

  ;; On the other hand, typed Scheme expects typed Scheme.
  (test-error (expand bindings '(* 1 (+ 2 3)))))

(test-end "curry")
